============
Tweeset
============

An open source project for hydrating and enriching a dataset of tweets.



Clone source code
=================

Tweeset source code is available in a Git repository. Make sure git is installed on your system and clone the Tweeset repository from Bitbucket. 

.. code-block:: bash

	git clone https://amjedbj@bitbucket.org/amjedbj/tweeset.git

You can clone the sources to any directory on your system. If you have no preferences, choose *~/tweeset* as that is the directory we will use below.


Compile project
===============

Let's assume you cloned  Tweeset repository like shown above to the directory *~/tweeset*. Make sure Java 1.7 and Maven  are installed on your system. Execute the following commands to compile the project.

.. code-block:: bash

	cd ~/tweeset
	mvn clean package appassembler:assemble


Configuration
==============

In order to use this tools you may have a Twitter developer account. After creating and validating your Twitter API application, you may add your credentials to `accounts.csv <accounts.csv>`_ file. 
You may also edit the `config.properties <config.properties>`_ file and modify parameters with respect of your needs.

Hydrate Statuses 
================

Hydrating statuses is especially useful to get the details of a collection of tweet IDs.

Let's assume that your tweet dataset is described in csv file *data/ids.csv* and tweet ids are available in field of index *1*.  The field index can be configured in `config.properties <config.properties>`_ file with *ids.csv.field.index* parameter.

Make sure you can write to current directory and start hydrating your tweet dataset using the next command line.

.. code-block:: bash

	target/appassembler/bin/HydrateStatuses --accounts accounts.csv --ids data/ids.csv

The previous command line will download and store raw JSON of statuses in cache directory. Furthermore, statuses will be saved in new line delimited JSON file *statuses.log* in the current working directory. A new *statuses.log* file is created at each hour while the previous one is compressed automatically.


Tweets that are not available are listed in logging file with default name *notfound.log*. You can change the path of the logging file by modifying option *log.status.notfound* in `config.properties <config.properties>`_ file.


Sample Statuses 
================

Sampling statuses enable to gather a random sample of all public statuses.  This feature calls `statuses/sample <https://dev.twitter.com/streaming/reference/get/statuses/sample>`_ method of Twitter API.
The next command line starts statuses sampling.

.. code-block:: bash

	target/appassembler/bin/SampleStatuses --accounts accounts.csv 

We note that statuses will be saved in new line delimited JSON file *statuses.log* in the current working directory. A new *statuses.log* file is created at each hour while the previous one is compressed automatically.



Filter Statuses 
================

Filtering statuses enable to filter public statuses based on users, tracking keywords, locations, etc.  This feature calls `statuses/filter <https://dev.twitter.com/streaming/reference/post/statuses/filter>`_ method of Twitter API.
It's expected to provide  argument *--params*  that point to file containing filtering parameters. An example of parameter file  is presented in `params.csv <params.csv>`_.
The next command line starts statuses filtering.

.. code-block:: bash

	target/appassembler/bin/FilterStatuses --accounts accounts.csv --params params.csv

We note that statuses will be saved in new line delimited JSON file *statuses.log* in the current working directory. A new *statuses.log* file is created at each hour while the previous one is compressed automatically.



Version
===============

Tweeset 1.0.0

Citation
===============

If you use this code, please cite this paper:

	Lynda Tamine, Laure Soulier, Lamjed Ben Jabeur, Frederic Amblard, Chihab Hanachi, Gilles Hubert, and Camille Roth. 2016. Social Media-Based Collaborative Information Access: Analysis of Online Crisis-Related Twitter Conversations. In Proceedings of the 27th ACM Conference on Hypertext and Social Media (HT '16). ACM, New York, NY, USA, 159-168. DOI: http://dx.doi.org/10.1145/2914586.2914589


Contributors
===============

The following people have contributed to this code:

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.

License
===============
This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.