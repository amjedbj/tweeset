package fr.irit.iris.tweeset;

import org.apache.log4j.Logger;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterObjectFactory;

public class TwitterStatusListener implements StatusListener {

	protected Logger logger = null;
	protected long count_statuses = 0l;

	public TwitterStatusListener() {

	}

	public TwitterStatusListener(Logger logger) {
		this.logger = logger;
	}

	protected void emmitStatus(Status status, Logger logger) {
		this.emmitStatus(TwitterObjectFactory.getRawJSON(status), logger);
	}

	protected void emmitStatus(String rawJson, Logger logger) {
		if (!rawJson.isEmpty()) {
			this.count_statuses++;
			logger.info(rawJson);
			if (this.count_statuses % 1000 == 0) {
				System.out.println(this.count_statuses + " messages received.");
			}
		}
	}

	@Override
	public void onException(Exception ex) {
	}

	@Override
	public void onStatus(Status status) {
		this.emmitStatus(status, this.logger);
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStallWarning(StallWarning arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		// TODO Auto-generated method stub

	}
}