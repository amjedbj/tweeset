package fr.irit.iris.tweeset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.rolling.RollingFileAppender;
import org.apache.log4j.rolling.TimeBasedRollingPolicy;
import org.apache.log4j.varia.LevelRangeFilter;

import twitter4j.FilterQuery;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.api.TweetsResources;
import twitter4j.conf.ConfigurationBuilder;

public class FilterStatuses {
	private static final String HOUR_ROLL = ".%d{yyyy-MM-dd-HH}.gz";

	public static final String CLI_OPTION_ACCOUNTS = "accounts";
	public static final String CLI_OPTION_PARAMS = "params";;

	public static final String CONFIG_FILE = "config.properties";

	public static final String OPTION_FOLLOW = "follow";
	public static final String OPTION_TRACK = "track";
	public static final String OPTION_LOCATIONS = "locations";
	public static final String OPTION_LANGUAGE = "language";

	public static final String HEADER_PARAM = "parameter";
	public static final String HEADER_Value = "value";

	public static final String HEADER_CONSUMER_KEY = "consumer_key";
	public static final String HEADER_CONSUMER_SECRET = "consumer_secret";
	public static final String HEADER_ACCESS_TOKEN = "access_token";
	public static final String HEADER_ACCESS_TOKEN_SECRET = "access_token_secret";
	protected boolean accountWithHeader = true;
	protected char accountWithDelimiter = '\t';

	protected boolean paramsWithHeader = true;
	protected char paramsWithDelimiter = '\t';

	protected int idsFieldIndex = 0;

	protected int count_statuses = 0;
	protected int maxLookup = 100;
	protected int cacheFolderLength = 8;

	protected int rateLimitSleep = 5;
	protected File cacheFolderPath = null;
	protected File notFoundFile = null;

	protected ConfigurationBuilder getConfigurationBuilder(Map<String, String> key) {

		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setJSONStoreEnabled(true);
		configurationBuilder.setOAuthConsumerKey(key.get(FilterStatuses.HEADER_CONSUMER_KEY).trim());
		configurationBuilder.setOAuthConsumerSecret(key.get(FilterStatuses.HEADER_CONSUMER_SECRET).trim());
		configurationBuilder.setOAuthAccessToken(key.get(FilterStatuses.HEADER_ACCESS_TOKEN).trim());
		configurationBuilder.setOAuthAccessTokenSecret(key.get(FilterStatuses.HEADER_ACCESS_TOKEN_SECRET).trim());
		return configurationBuilder;
	}

	protected List<TweetsResources> getResources(List<Map<String, String>> keys) {
		List<TweetsResources> resources = new ArrayList<TweetsResources>();
		for (Map<String, String> key : keys) {
			ConfigurationBuilder configurationBuilder = this.getConfigurationBuilder(key);
			TweetsResources resource = new TwitterFactory(configurationBuilder.build()).getInstance();
			resources.add(resource);
		}
		return resources;
	}

	protected List<TwitterStream> getStreams(List<Map<String, String>> keys) {
		List<TwitterStream> streams = new ArrayList<TwitterStream>();
		for (Map<String, String> key : keys) {
			ConfigurationBuilder configurationBuilder = this.getConfigurationBuilder(key);
			TwitterStream stream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
			streams.add(stream);
		}
		return streams;
	}

	protected CSVParser getCSVParser(File file, boolean withHeader, char withDelimiter) {
		try {
			Reader reader = new InputStreamReader(new FileInputStream(file));
			CSVFormat csvFormat = CSVFormat.EXCEL;
			if (withHeader)
				csvFormat = csvFormat.withHeader();
			csvFormat = csvFormat.withDelimiter(withDelimiter);
			CSVParser parser = new CSVParser(reader, csvFormat);
			return parser;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	protected Map<String, Object> getParams(File file) {
		Map<String, Object> params = new HashMap<String, Object>();
		try {

			CSVParser parser = this.getCSVParser(file, this.paramsWithHeader, this.paramsWithDelimiter);
			for (CSVRecord record : parser) {
				String param = record.get(FilterStatuses.HEADER_PARAM);
				String value = record.get(FilterStatuses.HEADER_Value);
				Object object = value;

				// FilterQuery query = new FilterQuery();
				if (param.equals(FilterStatuses.OPTION_FOLLOW)) {
					String[] strIds = value.trim().split("\\s*,\\s*");
					long[] ids = new long[strIds.length];
					int i = 0;
					for (String strId : strIds) {
						try {
							ids[i] = Long.parseLong(strId);
							i++;
						} catch (Exception e) {
						}
					}
					object = ids;
				}

				if (param.equals(FilterStatuses.OPTION_TRACK)) {
					object = value.trim().split("\\s*,\\s*");

				}
				if (param.equals(FilterStatuses.OPTION_LANGUAGE)) {
					object = value.trim().split("\\s*,\\s*");

				}

				if (param.equals(FilterStatuses.OPTION_LOCATIONS)) {
					String[] strLocations = value.trim().split(";");
					double[][] points = new double[strLocations.length][];
					int i = 0;
					for (String loc : strLocations) {
						String[] strPoints = loc.trim().split(",");
						double[] point = new double[strPoints.length];
						int j = 0;
						for (String strPoint : strPoints) {
							point[j] = Double.parseDouble(strPoint.trim());
							j++;
						}
						points[i] = point;
						i++;
					}
					object = points;
				}

				params.put(param, object);
			}
			parser.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return params;
	}

	protected Logger getLogger() {
		PatternLayout layoutStandard = new PatternLayout();
		layoutStandard.setConversionPattern("[%p] %d %c %M - %m%n");

		PatternLayout layoutSimple = new PatternLayout();
		layoutSimple.setConversionPattern("%m%n");

		// Filter for the statuses: we only want INFO messages
		LevelRangeFilter filter = new LevelRangeFilter();
		filter.setLevelMax(Level.INFO);
		filter.setLevelMin(Level.INFO);
		filter.setAcceptOnMatch(true);
		filter.activateOptions();

		TimeBasedRollingPolicy statusesRollingPolicy = new TimeBasedRollingPolicy();
		statusesRollingPolicy.setFileNamePattern("statuses.log" + HOUR_ROLL);
		statusesRollingPolicy.activateOptions();

		RollingFileAppender statusesAppender = new RollingFileAppender();
		statusesAppender.setRollingPolicy(statusesRollingPolicy);
		statusesAppender.addFilter(filter);
		statusesAppender.setLayout(layoutSimple);
		statusesAppender.activateOptions();

		TimeBasedRollingPolicy warningsRollingPolicy = new TimeBasedRollingPolicy();
		warningsRollingPolicy.setFileNamePattern("warnings.log" + HOUR_ROLL);
		warningsRollingPolicy.activateOptions();

		RollingFileAppender warningsAppender = new RollingFileAppender();
		statusesAppender.setRollingPolicy(statusesRollingPolicy);
		warningsAppender.setThreshold(Level.WARN);
		warningsAppender.setLayout(layoutStandard);
		warningsAppender.activateOptions();

		ConsoleAppender consoleAppender = new ConsoleAppender();
		consoleAppender.setThreshold(Level.WARN);
		consoleAppender.setLayout(layoutStandard);
		consoleAppender.activateOptions();

		// configures the root logger
		Logger rootLogger = Logger.getRootLogger();
		rootLogger.setLevel(Level.INFO);
		rootLogger.removeAllAppenders();
		rootLogger.addAppender(consoleAppender);
		rootLogger.addAppender(statusesAppender);
		rootLogger.addAppender(warningsAppender);

		// creates a custom logger and log messages
		Logger logger = Logger.getLogger(FilterStatuses.class);
		return logger;
	}

	protected List<Map<String, String>> getAccounts(File file) {
		List<Map<String, String>> keys = new ArrayList<Map<String, String>>();

		try {
			CSVParser parser = this.getCSVParser(file, this.accountWithHeader, this.accountWithDelimiter);
			for (CSVRecord record : parser) {
				Map<String, String> key = new HashMap<String, String>();
				key.put(FilterStatuses.HEADER_CONSUMER_KEY, record.get(FilterStatuses.HEADER_CONSUMER_KEY));
				key.put(FilterStatuses.HEADER_CONSUMER_SECRET, record.get(FilterStatuses.HEADER_CONSUMER_SECRET));
				key.put(FilterStatuses.HEADER_ACCESS_TOKEN, record.get(FilterStatuses.HEADER_ACCESS_TOKEN));
				key.put(FilterStatuses.HEADER_ACCESS_TOKEN_SECRET,
						record.get(FilterStatuses.HEADER_ACCESS_TOKEN_SECRET));
				keys.add(key);

			}
			parser.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return keys;

	}

	protected void run(File accountFile, File paramsFile) {

		List<Map<String, String>> keys = this.getAccounts(accountFile);
		List<TwitterStream> streams = this.getStreams(keys);
		Map<String, Object> params = this.getParams(paramsFile);
		Logger logger = this.getLogger();

		int k_resource = 0;
		int k_switch = 0;
		boolean switchkey = true;
		TwitterStream stream = streams.get(k_resource);
		while (switchkey) {
			switchkey = false;
			try {
				StatusListener StatusListener = new TwitterStatusListener(logger);
				stream.addListener(StatusListener);
				FilterQuery query = new FilterQuery();

				if (params.containsKey(FilterStatuses.OPTION_FOLLOW)) {
					query.follow((long[]) params.get(FilterStatuses.OPTION_FOLLOW));
				}

				if (params.containsKey(FilterStatuses.OPTION_TRACK)) {
					query.track((String[]) params.get(FilterStatuses.OPTION_TRACK));
				}
				if (params.containsKey(FilterStatuses.OPTION_LANGUAGE)) {
					query.track((String[]) params.get(FilterStatuses.OPTION_LANGUAGE));
				}
				if (params.containsKey(FilterStatuses.OPTION_LOCATIONS)) {
					query.locations((double[][]) params.get(FilterStatuses.OPTION_LOCATIONS));
				}
				stream.filter(query);

			} catch (NumberFormatException e) {
				System.err.println("Exception " + e.getClass().getSimpleName());
			} catch (Exception e) {
				System.err.println("Exception " + e.getClass().getSimpleName());
			}
			if (switchkey) {
				System.err.println("Switching key " + k_resource + ":\t"
						+ keys.get(k_resource).get(FilterStatuses.HEADER_CONSUMER_KEY));
				k_resource = (k_resource < keys.size() - 2) ? k_resource + 1 : 0;
				stream = streams.get(k_resource);
				k_switch++;
			}
			if (k_switch >= (keys.size() * 2)) {
				System.err.println("Sleeping for " + this.rateLimitSleep + " min");
				try {
					Thread.sleep(this.rateLimitSleep * 60 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				k_switch = 0;
			}
		}

	}

	public static void main(String[] args) throws IOException, InterruptedException, TwitterException {

		Options options = new Options();
		@SuppressWarnings("static-access")
		Option accounts = OptionBuilder.withArgName(FilterStatuses.CLI_OPTION_ACCOUNTS)
				.withLongOpt(FilterStatuses.CLI_OPTION_ACCOUNTS)
				.withDescription("CSV file  containing Twitter API accounts.").hasArg().isRequired(true).create("a");
		options.addOption(accounts);

		@SuppressWarnings("static-access")
		Option params = OptionBuilder.withArgName(FilterStatuses.CLI_OPTION_PARAMS)
				.withLongOpt(FilterStatuses.CLI_OPTION_PARAMS).withDescription("CSV file  containing filtering params.")
				.hasArg().isRequired(true).create("p");
		options.addOption(params);

		CommandLine cmdline = null;
		CommandLineParser parser = new GnuParser();
		try {
			cmdline = parser.parse(options, args);
		} catch (ParseException exp) {
			System.err.println("Error encountered while parsing command line: " + exp.getMessage());
			System.exit(-1);
		}

		if (!cmdline.hasOption(FilterStatuses.CLI_OPTION_ACCOUNTS)
				|| !cmdline.hasOption(FilterStatuses.CLI_OPTION_PARAMS)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(FilterStatuses.class.getName(), options);
			System.exit(-1);
		}

		File accountFile = new File(cmdline.getOptionValue(FilterStatuses.CLI_OPTION_ACCOUNTS));
		if (!accountFile.exists()) {
			System.err.println("Error: " + accountFile + " doesn't exist!");
			System.exit(-1);
		}

		File paramsFile = new File(cmdline.getOptionValue(FilterStatuses.CLI_OPTION_PARAMS));
		if (!paramsFile.exists()) {
			System.err.println("Error: " + paramsFile + " doesn't exist!");
			System.exit(-1);
		}
		FilterStatuses filtering = new FilterStatuses();
		filtering.run(accountFile, paramsFile);
	}
}
