/**
 * Copyright (C) 2016 IRIT (Lamjed.Ben-Jabeur@irit.fr)
 *
 * Licensed under the CeCILL-B License , Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.irit.iris.tweeset;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.rolling.RollingFileAppender;
import org.apache.log4j.rolling.TimeBasedRollingPolicy;
import org.apache.log4j.varia.LevelRangeFilter;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.api.TweetsResources;
import twitter4j.conf.ConfigurationBuilder;

public class HydrateStatuses {

	private static final String HOUR_ROLL = ".%d{yyyy-MM-dd-HH}.gz";

	public static final String CLI_OPTION_IDS = "ids";
	public static final String CLI_OPTION_ACCOUNTS = "accounts";

	public static final String CONFIG_FILE = "config.properties";

	public static final String HEADER_CONSUMER_KEY = "consumer_key";
	public static final String HEADER_CONSUMER_SECRET = "consumer_secret";
	public static final String HEADER_ACCESS_TOKEN = "access_token";
	public static final String HEADER_ACCESS_TOKEN_SECRET = "access_token_secret";

	protected boolean accountWithHeader = true;
	protected char accountWithDelimiter = '\t';
	protected boolean idsWithHeader = false;
	protected char idsWithDelimiter = '\t';
	protected int idsFieldIndex = 0;

	protected int count_statuses = 0;
	protected int maxLookup = 100;
	protected int cacheFolderLength = 8;

	protected int rateLimitSleep = 5;
	protected File cacheFolderPath = null;
	protected File notFoundFile = null;

	public HydrateStatuses() {
		this.init();
	}

	private void storeStatus(Status status, File file) {
		FileOutputStream outputStream = null;
		OutputStreamWriter streamWriter = null;
		BufferedWriter writer = null;
		try {
			outputStream = new FileOutputStream(file);
			streamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			writer = new BufferedWriter(streamWriter);
			writer.write(TwitterObjectFactory.getRawJSON(status));
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException ignore) {
				}
			}
			if (streamWriter != null) {
				try {
					streamWriter.close();
				} catch (IOException ignore) {
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException ignore) {
				}
			}
		}
	}

	private void emmitStatus(Status status, Logger logger) {
		this.emmitStatus(TwitterObjectFactory.getRawJSON(status), logger);
	}

	private void emmitStatus(String rawJson, Logger logger) {
		if (!rawJson.isEmpty()) {
			this.count_statuses++;
			logger.info(rawJson);
			if (this.count_statuses % 1000 == 0) {
				System.out.println(this.count_statuses + " messages received.");
			}
		}
	}

	private Set<Long> lookup(List<Long> loopkupIds, File cacheStatusesDirectory, TweetsResources resource,
			Logger logger) throws IOException, TwitterException {
		ResponseList<Status> statuses = resource.lookup(this.toArray(loopkupIds));
		Set<Long> foundedStatuses = new HashSet<Long>();
		for (Status status : statuses) {
			File file = this.getCacheFile(cacheStatusesDirectory, Long.toString(status.getId()));
			this.storeStatus(status, file);
			this.emmitStatus(status, logger);
			foundedStatuses.add(status.getId());
			this.count_statuses++;
		}
		return foundedStatuses;
	}

	private void emmitNotFoundStatuses(List<Long> loopkupIds, Set<Long> foundedStatuses, File notFoundFile)
			throws IOException {
		for (long id : loopkupIds) {
			if (!foundedStatuses.contains(id)) {
				FileUtils.write(notFoundFile, Long.toString(id) + System.getProperty("line.separator"), "UTF-8", true);
			}
		}
	}

	protected void init() {
		Properties properties = new Properties();
		try {
			InputStream input = HydrateStatuses.class.getClassLoader().getResourceAsStream(HydrateStatuses.CONFIG_FILE);
			input = (input != null) ? input : new FileInputStream(new File(HydrateStatuses.CONFIG_FILE));
			properties.load(input);
			this.accountWithHeader = properties.containsKey("acount.csv.with_header")
					? properties.getProperty("acount.csv.with_header").equals("true") : this.accountWithHeader;
			this.accountWithDelimiter = (char) (properties.containsKey("acount.csv.with_delimiter")
					? (properties.getProperty("acount.csv.with_delimiter").length() > 0
							? properties.getProperty("acount.csv.with_delimiter").charAt(0) : this.accountWithDelimiter)
					: this.accountWithDelimiter);

			this.idsWithHeader = properties.containsKey("acount.csv.with_header")
					? properties.getProperty("acount.csv.with_header").equals("true") : this.idsWithHeader;
			this.idsWithDelimiter = (char) (properties.containsKey("ids.csv.with_delimiter")
					? (properties.getProperty("ids.csv.with_delimiter").length() > 0
							? properties.getProperty("ids.csv.with_delimiter").charAt(0) : this.idsWithDelimiter)
					: this.idsWithDelimiter);

			this.idsFieldIndex = properties.containsKey("ids.csv.field.index")
					? Integer.parseInt(properties.getProperty("ids.csv.field.index")) : this.idsFieldIndex;

			this.maxLookup = properties.containsKey("status.lookup.max")
					? Integer.parseInt(properties.getProperty("status.lookup.max")) : this.maxLookup;
			this.cacheFolderLength = properties.containsKey("cache.folder.length")
					? Integer.parseInt(properties.getProperty("cache.folder.length")) : this.cacheFolderLength;

			this.cacheFolderPath = properties.containsKey("cache.folder.path")
					? new File(properties.getProperty("cache.folder.path")) : new File("cache/twitter/status");

			this.notFoundFile = properties.containsKey("log.status.notfound")
					? new File(properties.getProperty("log.status.notfound")) : new File("notfound.log");

			this.rateLimitSleep = properties.containsKey("api.ratelimit.sleep")
					? Integer.parseInt(properties.getProperty("api.ratelimit.sleep")) : this.rateLimitSleep;

			input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected long[] toArray(List<Long> list) {
		long[] array = new long[list.size()];
		int i = 0;
		for (long item : list) {
			array[i] = item;
			i++;
		}
		return array;
	}

	protected List<Map<String, String>> getAccounts(File file) {
		List<Map<String, String>> keys = new ArrayList<Map<String, String>>();

		try {
			CSVParser parser = this.getCSVParser(file, this.accountWithHeader, this.accountWithDelimiter);
			for (CSVRecord record : parser) {
				Map<String, String> key = new HashMap<String, String>();
				key.put(HydrateStatuses.HEADER_CONSUMER_KEY, record.get(HydrateStatuses.HEADER_CONSUMER_KEY));
				key.put(HydrateStatuses.HEADER_CONSUMER_SECRET, record.get(HydrateStatuses.HEADER_CONSUMER_SECRET));
				key.put(HydrateStatuses.HEADER_ACCESS_TOKEN, record.get(HydrateStatuses.HEADER_ACCESS_TOKEN));
				key.put(HydrateStatuses.HEADER_ACCESS_TOKEN_SECRET,
						record.get(HydrateStatuses.HEADER_ACCESS_TOKEN_SECRET));
				keys.add(key);

			}
			parser.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return keys;

	}

	protected ConfigurationBuilder getConfigurationBuilder(Map<String, String> key) {

		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setJSONStoreEnabled(true);
		configurationBuilder.setOAuthConsumerKey(key.get(HydrateStatuses.HEADER_CONSUMER_KEY).trim());
		configurationBuilder.setOAuthConsumerSecret(key.get(HydrateStatuses.HEADER_CONSUMER_SECRET).trim());
		configurationBuilder.setOAuthAccessToken(key.get(HydrateStatuses.HEADER_ACCESS_TOKEN).trim());
		configurationBuilder.setOAuthAccessTokenSecret(key.get(HydrateStatuses.HEADER_ACCESS_TOKEN_SECRET).trim());
		return configurationBuilder;
	}

	protected List<TweetsResources> getResources(List<Map<String, String>> keys) {
		List<TweetsResources> resources = new ArrayList<TweetsResources>();
		for (Map<String, String> key : keys) {
			ConfigurationBuilder configurationBuilder = this.getConfigurationBuilder(key);
			TweetsResources resource = new TwitterFactory(configurationBuilder.build()).getInstance();
			resources.add(resource);
		}
		return resources;
	}

	protected CSVParser getCSVParser(File file, boolean withHeader, char withDelimiter) {
		try {
			Reader reader = new InputStreamReader(new FileInputStream(file));
			CSVFormat csvFormat = CSVFormat.EXCEL;
			if (withHeader)
				csvFormat = csvFormat.withHeader();
			csvFormat = csvFormat.withDelimiter(withDelimiter);
			CSVParser parser = new CSVParser(reader, csvFormat);
			return parser;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	protected Logger getLogger() {
		PatternLayout layoutStandard = new PatternLayout();
		layoutStandard.setConversionPattern("[%p] %d %c %M - %m%n");

		PatternLayout layoutSimple = new PatternLayout();
		layoutSimple.setConversionPattern("%m%n");

		// Filter for the statuses: we only want INFO messages
		LevelRangeFilter filter = new LevelRangeFilter();
		filter.setLevelMax(Level.INFO);
		filter.setLevelMin(Level.INFO);
		filter.setAcceptOnMatch(true);
		filter.activateOptions();

		TimeBasedRollingPolicy statusesRollingPolicy = new TimeBasedRollingPolicy();
		statusesRollingPolicy.setFileNamePattern("statuses.log" + HOUR_ROLL);
		statusesRollingPolicy.activateOptions();

		RollingFileAppender statusesAppender = new RollingFileAppender();
		statusesAppender.setRollingPolicy(statusesRollingPolicy);
		statusesAppender.addFilter(filter);
		statusesAppender.setLayout(layoutSimple);
		statusesAppender.activateOptions();

		TimeBasedRollingPolicy warningsRollingPolicy = new TimeBasedRollingPolicy();
		warningsRollingPolicy.setFileNamePattern("warnings.log" + HOUR_ROLL);
		warningsRollingPolicy.activateOptions();

		RollingFileAppender warningsAppender = new RollingFileAppender();
		statusesAppender.setRollingPolicy(statusesRollingPolicy);
		warningsAppender.setThreshold(Level.WARN);
		warningsAppender.setLayout(layoutStandard);
		warningsAppender.activateOptions();

		ConsoleAppender consoleAppender = new ConsoleAppender();
		consoleAppender.setThreshold(Level.WARN);
		consoleAppender.setLayout(layoutStandard);
		consoleAppender.activateOptions();

		// configures the root logger
		Logger rootLogger = Logger.getRootLogger();
		rootLogger.setLevel(Level.INFO);
		rootLogger.removeAllAppenders();
		rootLogger.addAppender(consoleAppender);
		rootLogger.addAppender(statusesAppender);
		rootLogger.addAppender(warningsAppender);

		// creates a custom logger and log messages
		Logger logger = Logger.getLogger(HydrateStatuses.class);
		return logger;
	}

	protected File getCacheFolder(File directory, String idStr) {
		File cacheFolder = new File(
				directory.getPath() + "/" + idStr.substring(0, Math.min(this.cacheFolderLength, idStr.length())));
		if (!cacheFolder.exists()) {
			cacheFolder.mkdirs();
		}
		return cacheFolder;
	}

	protected File getCacheFile(File directory, String idStr) {
		File cacheFolder = this.getCacheFolder(directory, idStr);
		File cacheFile = new File(cacheFolder.getPath() + "/" + idStr + ".json");
		return cacheFile;
	}

	public void qrun(File accountFile, File idsFile) {

		File cacheStatusesDirectory = new File(this.cacheFolderPath.getPath() + "/200");
		List<Map<String, String>> keys = this.getAccounts(accountFile);
		List<TweetsResources> resources = this.getResources(keys);
		Logger logger = this.getLogger();

		int k_resource = 0;
		int k_lookup = 0;

		TweetsResources resource = resources.get(k_resource);
		CSVParser parser = this.getCSVParser(idsFile, this.idsWithHeader, this.idsWithDelimiter);

		List<Long> loopkupIds = new ArrayList<Long>();
		for (CSVRecord record : parser) {
			String id_str = record.get(this.idsFieldIndex);

			File cacheFile = this.getCacheFile(cacheStatusesDirectory, id_str);

			if (cacheFile.exists()) {
				try {
					String rawJSON = FileUtils.readFileToString(cacheFile, "UTF-8");
					this.emmitStatus(rawJSON, logger);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				loopkupIds.add(Long.parseLong(id_str));
			}

			if (loopkupIds.size() >= this.maxLookup) {
				boolean switchkey = true;
				int k_switch = 0;
				while (switchkey) {
					switchkey = false;
					try {
						k_lookup++;
						System.out.println("lookup statuses\t" + k_lookup);
						Set<Long> foundedStatuses = this.lookup(loopkupIds, cacheStatusesDirectory, resource, logger);
						this.emmitNotFoundStatuses(loopkupIds, foundedStatuses, this.notFoundFile);

						loopkupIds.clear();
					} catch (TwitterException e) {
						System.err.println("TwitterException " + e.getErrorCode());
						if (e.getErrorCode() == 32) {
							switchkey = true;
						}
						if (e.getErrorCode() == 88) {
							System.err.println("Limit Rate exceeded");
							switchkey = true;
						}
					} catch (NumberFormatException e) {
						System.err.println("Exception " + e.getClass().getSimpleName());
					} catch (Exception e) {
						System.err.println("Exception " + e.getClass().getSimpleName());
					}
					if (switchkey) {
						System.err.println("Switching key " + k_resource + ":\t"
								+ keys.get(k_resource).get(HydrateStatuses.HEADER_CONSUMER_KEY));
						k_resource = (k_resource < keys.size() - 2) ? k_resource + 1 : 0;
						resource = resources.get(k_resource);
						k_switch++;
					}
					if (k_switch >= (keys.size() * 2)) {
						System.err.println("Sleeping for " + this.rateLimitSleep + " min");
						try {
							Thread.sleep(this.rateLimitSleep * 60 * 1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						k_switch = 0;
					}
				}

				loopkupIds.clear();
			}
		}
		try {
			parser.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException, TwitterException {

		Options options = new Options();
		@SuppressWarnings("static-access")
		Option accounts = OptionBuilder.withArgName(HydrateStatuses.CLI_OPTION_ACCOUNTS)
				.withLongOpt(HydrateStatuses.CLI_OPTION_ACCOUNTS)
				.withDescription("CSV file  containing Twitter API accounts.").hasArg().isRequired(true).create("a");
		options.addOption(accounts);

		@SuppressWarnings("static-access")
		Option ids = OptionBuilder.withArgName(HydrateStatuses.CLI_OPTION_IDS)
				.withLongOpt(HydrateStatuses.CLI_OPTION_IDS).withDescription("File listing ids of Twitter statuses.")
				.hasArg().isRequired(true).create("i");
		options.addOption(ids);

		CommandLine cmdline = null;
		CommandLineParser parser = new GnuParser();
		try {
			cmdline = parser.parse(options, args);
		} catch (ParseException exp) {
			System.err.println("Error encountered while parsing command line: " + exp.getMessage());
			System.exit(-1);
		}

		if (!cmdline.hasOption(HydrateStatuses.CLI_OPTION_ACCOUNTS)
				|| !cmdline.hasOption(HydrateStatuses.CLI_OPTION_IDS)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(HydrateStatuses.class.getName(), options);
			System.exit(-1);
		}

		File accountFile = new File(cmdline.getOptionValue(HydrateStatuses.CLI_OPTION_ACCOUNTS));
		if (!accountFile.exists()) {
			System.err.println("Error: " + accountFile + " doesn't exist!");
			System.exit(-1);
		}

		File idsFile = new File(cmdline.getOptionValue(HydrateStatuses.CLI_OPTION_IDS));
		if (!idsFile.exists()) {
			System.err.println("Error: " + idsFile + " doesn't exist!");
			System.exit(-1);
		}

		System.out.println("account " + HydrateStatuses.CLI_OPTION_ACCOUNTS + " "
				+ cmdline.getOptionValue(HydrateStatuses.CLI_OPTION_ACCOUNTS));
		System.out.println("idsFile " + HydrateStatuses.CLI_OPTION_IDS + " "
				+ cmdline.getOptionValue(HydrateStatuses.CLI_OPTION_IDS));

		HydrateStatuses gather = new HydrateStatuses();
		gather.qrun(accountFile, idsFile);
	}
}
